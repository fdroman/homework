package ru.fedorov.Lesson21;

//  ДЗ_21_1_2_Реверс массива
//  Задача: Имеется массив, нужно переставить элементы массива в обратном порядке.
//  Задачу не зачитывать если использованы утильные методы класса Arrays.
//  Решением также не являются манупуляции с выводом массива. Необходимо действительно перемещать элементы.
//  Вывести массив в консоль до и после вызова метода по реверсу массива

import java.util.Arrays;
//import ru.fedorov.ForLessonPackage.

import ru.fedorov.ForLessonPackage.MetodForHomework;

public class ArrayReverse {
    private static final int[] array = {11, 22, 33, 44, 55, 66, 77, 88, 99};

    public static void main(String[] args) {

        showArray("Было");   //вывод в консоль исходного массива
        MetodForHomework.ArrayReverse(array);
        showArray("Стало: ");   //вывод в консоль нового массива

    }


    private static void showArray(String a) {
        System.out.println(a + ": " + Arrays.toString(array));
    }
}
