package ru.fedorov.Lesson21;

//  ДЗ_21_1_Сдвиг элементов массива
//  Дан двумерный массив. Задача – написать метод
//  public void toLeft() {}
//  1.Пройти с 1-ой до последней строки
//  2.Пройти с 1-го до предпоследнего элемента
//  3.В текущую ячейку поместить значение следующей
//  4.Последнему элементу присвоить 0
//  Так выглядит любая строка после преобразования данным методом
//  2 3 4 5 6 7 8 9 10 0

public class ArrayLeftShift {


    private static final int[][] array = {
            {11, 12, 13, 14, 15, 16, 17, 18, 19, 20},
            {21, 22, 23, 24, 25, 26, 27, 28, 29, 30}
    };

    public static void main(String[] args) {
        String a = "Было";
        String b = "Стало";
        showArray(a);
        arrayLeftShift();
        showArray(b);
    }

    private static void arrayLeftShift() {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length - 1; j++) {
                array[i][j]=array[i][j+1];
            }
            array[i][array[i].length - 1] = 0;
        }
    }

    private static void showArray(String a) {
        System.out.println(a+ ":");
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

}

