package ru.fedorov.Lesson13.HomeWork;

public class FoodException extends RuntimeException {
    public FoodException(String message) {
        super(message);
    }
}
