package ru.fedorov.Lesson13.HomeWork;

public class App {

    public static void main(String[] args) {

        System.out.println("ОБЕД \nна стол Мама поставила:\n");
        Mama mama = new Mama();
        int[] dinnerMama = mama.menuFeed();    // * Мама формирует перечень блюд для подачи ребёнку

        Menu menu = new Menu();
        String[] listMenu = menu.getFood();
        for (int i = 0; i < dinnerMama.length; i++) {   //выводится перечень блюд установленных на стол
            System.out.println("Блюдо №" + (i + 1) + ": " + listMenu[dinnerMama[i]]);
        }

        System.out.println("\nРебёнок начинает есть:\n");

        Child child = new Child();
        //int j = dinnerMama.length;
        boolean like;
        for (int value : dinnerMama) { //отработка каждого конкретного блюда
            try {
                if (child.eat(value, dinnerMama.length)) {
                    System.out.print("съел <" + listMenu[value] + "> за обе щёки! ");
                    //throw new FoodException("съел " + listMenu[dinnerMama[i]] + " за обе щёки!");
                } else {
                    //System.out.print("Нет! я не буду <" + listMenu[value] + "> есть сейчас! ");
                    throw new FoodException("Нет! я не буду <" + listMenu[value] + "> есть сейчас");

                }
            } catch (FoodException er) {
                er.printStackTrace();
                System.out.println(er.getMessage());
            } finally {
                System.out.print("Спасибо Мама\n\n");
            }
        }
    }
}


//        for (int i = 0; i < dinnerMama.length; i++) {
//            int j = dinnerMama[i];
//            System.out.println("Блюдо №" + (i + 1) + ": " + MessageFormat.format("{" + j + "}", (Object[]) menu.getFood()));// ** вывод на экран списка блюд
//            //          System.out.println(MessageFormat.format("-=-{1}", (Object[]) menu.getFood()));
//        }


//        for (int i = 0; i < (Integer) numberInt ; i++) {
//            System.out.println("=======");
//
//        }

//        Child child = new Child();
//        child.eat();    //формируется список вкусовых предпочтений из всего списка

//метод try нравися/не нравится каждое конкретное блюдо


//System.out.println(MessageFormat.format("-=-{1}", (Object[]) menu.getFood()));
// ** System.out.printf("-=-%s%n", menu.getFood());

//        Object numberInt = MetodForHomework.intFromConsole("Введите число - сколько блюд на обед подаст Мама ребёнку:");
//        System.out.println(numberInt);

//   int[] dinner = (int[]) MetodForHomework.intNumberRandomAB(z, 1, 5);

// Object mass = MetodForHomework.intNumberRandomAB(z, -10, 10);