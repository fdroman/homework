package ru.fedorov.Lesson13.HomeWork;

public class Menu2 {
    private String[] food = {
            "салат летний",
            "салат греческий",
            "салат столичный",
            "окрошка",
            "щи",
            "борщ",
            "овсянка",
            "макароны",
            "запеканка",
            "омлет",
            "тушёные овощи",
            "яичница",
            "йогурт",
            "фрукты"
    };

    public String[] getFood() {
        return food;
    }
}

