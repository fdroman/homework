package ru.fedorov.Lesson13.HomeWork_old;

import java.util.Scanner;

public class BabyEating {
    public static void main(String[] args) {
        boolean e = false;
        int i = 0;
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ребёнок сел за стол, ");
        System.out.println("выберите действие:");
        System.out.println("кормить:       food");
        System.out.println("закончить обед: end");

        while (scanner.hasNext()) {
            String act = scanner.next();

            switch (act) {
                case "food":
                    Menu randomMenu = Menu.getRandom();
                    try {
                        double random = Math.random();
                        if (random < 0.3) {
                            throw new FoodException("я не хочу " + randomMenu.food + " сейчас");
                        } else {
                            if (i > 6) {
                                throw new FoodException("я наелся, я съел уже " + i + " продуктов");
                            }
                            System.out.print("МММММ " + randomMenu.food + " ..... очень вкусно! ");
                            i = i + 1;
                        }
                    } catch (FoodException ex) {
                        ex.printStackTrace();
                        System.out.println(ex.getMessage());
                    } finally {
                        System.out.println("СПАСИБО!");
                    }
                    e = true;
                    break;
                case "end":
                    if (!e) {
                        System.out.println("я ещё на кушал! дай мне что нибудь на обед");
                        break;
                    }
                    e = true;
                    System.out.println("обед закончен.\nCпасибо, мне понравилось.");
                    return;
                default:
                    System.out.println("неизвестная команда. food, или end");
            }
        }
    }
}
