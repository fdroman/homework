package ru.fedorov.Lesson13.HomeWork_old;

public enum Menu {
    BURGER("бургер"),
    SOUP("суп"),
    CUTLET("котлета"),
    JUICE("сок"),
    MILK("молоко"),
    EGG("яйцо"),
    PORRIDGE("каша"),
    APPLE("яблоко"),
    YOGURT("йогурт"),
    OMELETTE("омлет"),
    OKROSHKA("окрошка"),
    BORSCHT("борщ"),
    BANANA("банан");


    public void setFood(String food) {
        this.food = food;
    }

    Menu(String food) {
        this.food = food;
    }

    String food;

    public static Menu getRandom() {
        return values()[(int) (Math.random() * values().length)];
    }
}
