package ru.fedorov.Lesson13.HomeWork_new;

import ru.fedorov.Lesson13.HomeWork.FoodException;

public class Mama {

    public void eat() {

        Food randomFood = Food.getRandom();
        String eat = randomFood.food;
        System.out.println("Обед на столе, иди кушать <" + eat + ">");
        try {
            Child.eat(eat);
        } catch (FoodException er) {
            er.printStackTrace();
        }
    }
}

