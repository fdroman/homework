package ru.fedorov.Lesson13.HomeWork_new;

public class FoodException extends RuntimeException {
    public FoodException(String message) {
        super(message);
    }
}
