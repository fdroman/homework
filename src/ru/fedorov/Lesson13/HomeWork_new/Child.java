package ru.fedorov.Lesson13.HomeWork_new;

public class Child {
    public static void eat(String eat) throws FoodException {
        //вся еда вкусная кроме СУП и ОВСЯНКА
        try {
            if (eat.equals(Food.OATMEAL.getFood()) || eat.equals(Food.SOUP.getFood())) {
                throw new FoodException("еда <" + eat + "> моя не любимая еда!");
            }
            System.out.println("Съел <" + eat + "> за обе щёки");
        } finally {
            System.out.println("Спасибо Мама!");
        }
    }
}

