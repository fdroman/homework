package ru.fedorov.Lesson13.HomeWork_new;

public enum Food {
    SALAD("салат"),
    OKROSHKA("окрошка"),
    SOUP("суп"),
    OATMEAL("овсянка"),
    PASTA("макароны"),
    APPLE("яблоко");


    String food;


    Food(String food) {
        this.food = food;
    }

    public String getFood() {
        return food;
    }

    public static Food getRandom() {
        return values()[(int) (Math.random() * values().length)];
    }

}
