package ru.fedorov.Lesson13.Study;

public class FinallyDemo {
    public static void main(String[] args) {
        try {
            doSome();
        } catch (RuntimeException e) {
            System.out.println("Ошибка <" + e.getMessage() +
                    "> обработана");
        }
        System.out.println("Программа завершена");
    }

    private static void doSome() {
        System.out.println("варю суп");
        try {
            doSomeElse();
            // не выполняется при ошибке
            System.out.println("суп готов");
        } finally {
            // выполняется всегда
            System.out.println("выключил плиту");
        }
        // не выполняется при ошибке
        System.out.println("приглашаю всех к столу");
    }

    private static void doSomeElse() {
        throw new RuntimeException("Моя ошибка");
    }
}
