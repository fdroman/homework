package ru.fedorov.Lesson13.Study;

public class ExceptionDemo {

    public static void main(String[] args) {
        try {
            try {
                doSome();
            } catch (FirstChildException e) {
                System.out.println("First child Exception!");
            } catch (SecondChildException e) {
                System.out.println("Second child Exception!");
                throw e;
            } finally {
                System.out.println("doSome выполнился");
            }
        } catch (MyParentException e) {
            System.out.println("Parent Exception!");
            e.printStackTrace();
        } catch (MyRootException e) {
            System.out.println("Root Exception!");
            e.printStackTrace();
        } catch (Exception e) {
            System.out.println("Any other exception");
            e.printStackTrace();
        }
        System.out.println("done");
    }

    private static void doSome() throws MyRootException {
        throw new SecondChildException();
    }
}

class MyRootException extends Exception {
}

class MyParentException extends MyRootException {
}

class AnotherParentException extends MyRootException {
}

class FirstChildException extends MyParentException {
}

class SecondChildException extends MyParentException {
}
