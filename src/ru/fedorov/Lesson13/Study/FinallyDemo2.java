package ru.fedorov.Lesson13.Study;

public class  FinallyDemo2 {
    public static void main(String[] args) {
        try {
            String s = doSome();
            System.out.println(s);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String doSome() {
        RuntimeException thrown = null;
        try {
            doElse();
            return "Good!";
        } catch (Exception e) {
            //return "Bad!";
            thrown = new RuntimeException(e);
        } finally {
            //return "Perfect!";

            RuntimeException e = new RuntimeException();
            if (thrown != null) {
                thrown.addSuppressed(e);
            }
            throw thrown;
        }
    }

    private static void doElse() {
        throw new RuntimeException();
    }
}

