package ru.fedorov.Lesson13.Study;

import java.io.IOException;

public class InheritanceDemo {
    public static void main(String[] args) {
        Root root = new Root();
        try {
            root.doSome();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

class Root {
    void doSome() throws IOException {
    }
}

class Child extends Root{
    @Override
    void doSome() {
    }
}
