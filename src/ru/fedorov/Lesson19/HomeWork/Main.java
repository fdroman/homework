package ru.fedorov.Lesson19.HomeWork;
//        Кассовый чек
//        Дан текстовый файл определенной структуры, в котором содержится информация о покупках.
//        Формат файла: Название товара, количество, цена
//        Необходимо написать программу, которая выведет на экран чек, сформированный из этого файла. В чеке должна быть информация:
//        название товара:  цена Х кол-во = стоимость
//        В конце отчета вывести итоговую стоимость.
//        Пример входного файла и вывода прикрепляю к задаче
//        out.txt
//        products.txt

import java.io.FileReader;
import java.util.Formatter;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String input = "src/ru/fedorov/lesson19/HomeWork/products.txt";

        try (FileReader reader = new FileReader(input);
             Scanner scanner = new Scanner(reader)) {
            Formatter formatter = new Formatter();

            formatter.format("%-17s %6s %1s %-6s %12s%n", "Наименование", "Цена", " ", "Кол-во", " Стоимость");

            for (int i = 1; i < 48; i++) {
                formatter.format("=");
            }
            formatter.format("%n");

            float sum;
            float result = 0;

            while (scanner.hasNext()) {
                String name = scanner.nextLine();
                float weight = Float.parseFloat(scanner.nextLine());
                float cost = Float.parseFloat(scanner.nextLine());

                sum = weight * cost;
                result = result + sum;
                formatter.format("%-18s", name);
                formatter.format("%6.2f", cost);
                formatter.format("%s", " x ");
                formatter.format("%6.3f", weight);
                formatter.format("%14s\n", String.format("=%.2f", sum));
            }

            for (int i = 1; i < 48; i++) {
                formatter.format("=");
            }
            formatter.format("%n%-38s %6.2f", "Итого:", result);

            System.out.println(formatter);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}