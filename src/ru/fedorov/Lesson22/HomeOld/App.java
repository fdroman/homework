package ru.fedorov.Lesson22.HomeOld;

//Реализация сложного компаратора
//Написать класс PersonSuperComparator,
//который имплементит Comparator, но умеет сравнивать по двум параметрам , возраст и имя.
//Класс Person теперь содержит два поля int age и String name

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class App {
    public static void main(String[] args) {
        List<Person> person = new ArrayList<>();
        person.add(new Person("Иван", 25));
        person.add(new Person("Наташа", 25));
        person.add(new Person("Иван", 23));
        person.add(new Person("Анастасия", 20));
        person.add(new Person("Миша", 28));
        person.add(new Person("Артём", 25));

        show("Было: ",person);

        Collections.sort(person, new PersonSuperComparator());

        show("Стало: ", person);
    }

    private static void show(String text, List<Person> person) {
        System.out.println(text);
        for (Person Person : person) {
            System.out.println(Person.getName() + " " + Person.getAge());
        }
        System.out.println();
    }

}

