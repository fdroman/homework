package ru.fedorov.Lesson22.Home;

import java.util.Comparator;

public class PersonSuperComparator implements Comparator<Person> {

    public int compare(Person p1, Person p2) {
        String name1 = p1.getName();
        String name2 = p2.getName();
        int result = name1.compareTo(name2);

        if (result != 0) {
            return result;
        }

        Integer age1 = p1.getAge();
        Integer age2 = p2.getAge();
        return age1.compareTo(age2);
    }
}

