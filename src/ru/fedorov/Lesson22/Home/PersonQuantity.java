package ru.fedorov.Lesson22.Home;

public enum  PersonQuantity {

    NAME1("Иван", 25),
    NAME2("Иван", 24),
    NAME3("Иван", 23),
    NAME4("Иван", 22),
    NAME5("Василий", 24),
    NAME6("Генадий", 28),
    NAME7("Николай", 34),
    NAME8("Антон", 27),
    NAME9("Иван", 44),
    NAME10("Гоша", 20),
    NAME11("Тимур", 23),
    NAME12("Илья", 56),
    NAME13("Роман", 27),
    NAME14("Роман", 22),
    NAME15("Наташа", 20),
    NAME16("Катя", 26),
    NAME17("Вика", 23),
    NAME18("Мирина", 22),
    NAME19("Юля", 27);


    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    private String name;
    private int age;

    PersonQuantity(String name, int age){
        this.name = name;
        this.age = age;
    }

}
