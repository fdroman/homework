package ru.fedorov.Lesson22.Home;

//Реализация сложного компаратора.
//Написать класс PersonSuperComparator,
//который имплементит Comparator, но умеет сравнивать по двум параметрам , возраст и имя.
//Класс Person теперь содержит два поля int age и String name

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class App {
    public static void main(String[] args) {
        List<Person> person = new ArrayList<>();
        for (PersonQuantity s : PersonQuantity.values()) {
            person.add(new Person(s.getName(), s.getAge()));
        }

        show("Было: ", person);

        Collections.sort(person, new PersonSuperComparator());

        show("Стало: ", person);
    }

    private static void show(String text, List<Person> person) {
        System.out.println(text);
        for (Person Person : person) {
            System.out.println(Person.getName() + " " + Person.getAge());
        }
        System.out.println();
    }

}


//       // Collections.sort(…)
//        List<ObjectName> list = new ArrayList<ObjectName>();
//        Collections.sort(list, new Comparator<ObjectName>() {
//          public int compare(ObjectName o1, ObjectName o2) {
//              return o1.toString().compareTo(o2.toString());
//        }
//        });