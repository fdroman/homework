package ru.fedorov.Lesson8;

//**
public class Counter {
    public static int counter = 0;

    Counter() {
        count();
    }

    public void count() {
        counter++;
        System.out.println("побывали в методе count " + counter + " раз.");
    }

    public static void main(String[] args) {
        Counter object1 = new Counter();
        Counter object2 = new Counter();
        Counter object3 = new Counter();

        System.out.println("Количество созданных объектов = " + Counter.counter);
    }


}
