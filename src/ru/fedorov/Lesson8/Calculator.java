package ru.fedorov.Lesson8;
//*
import java.util.Scanner;

public class Calculator {

    static Scanner scan = new Scanner(System.in);

    public static void main(String[] args) {
        double result = 0;
        System.out.print("Введите первое число:");
        double numberOne = scan.nextDouble();

        System.out.print("Введите операцию:");
        char operation = scan.next().charAt(0);


        System.out.print("Введите второе число:");
        double numberTwo = scan.nextDouble();

        switch (operation) {
            case '+':
                result = numberOne + numberTwo;
                break;
            case '-':
                result = numberOne - numberTwo;
                break;
            case '*':
                result = numberOne * numberTwo;
                break;
            case '/':
                result = numberOne / numberTwo;
                break;
            case '%':
                result = ((numberOne * 100) / numberTwo);
                break;
            default:
                System.out.println("ОШИБКА ВВОДА");
        }
        System.out.println(numberOne + " " + operation + " " + numberTwo + " = " + result);
    }
}