package ru.fedorov.Lesson8.DogovjrAct;

import java.util.Arrays;
import java.util.Date;

public class Act {
    private int number;
    private Date date;
    private String product [];

    public Act(int number, Date date, String[] product) {
        this.number = number;
        this.date = date;
        this.product = product;
    }

    @Override
    public String toString() {
        return "Act " +
                "number='" + number + '\'' +
                ", date=" + date +
                ", product=" + Arrays.toString(product);
    }
}
