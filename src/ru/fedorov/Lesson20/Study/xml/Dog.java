package ru.fedorov.Lesson20.Study.xml;


import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Dog {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Dog{" +
                "name='" + name + '\'' +
                '}';
    }
}
