package ru.fedorov.Lesson20.Study.xml;

import javax.xml.bind.annotation.*;
import java.util.Arrays;

@XmlRootElement(name = "Animal")
public class Cat extends Animal {
    private String secret;
    private int age;
    private String[] games;

    public Cat(String name, int age, String[] games) {
        setName(name);
        this.age = age;
        this.games = games;
    }

    public Cat() {
    }

    @XmlElement
    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @XmlElementWrapper
    @XmlElement(name = "value")
    public String[] getGames() {
        return games;
    }

    public void setGames(String[] games) {
        this.games = games;
    }

    @XmlTransient
    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    @Override
    public String toString() {
        return "Cat{" +
                "name='" + getName() + '\'' +
                ", age=" + age +
                ", games=" + Arrays.toString(games) +
                ", secret=" + secret +
                '}';
    }
}
