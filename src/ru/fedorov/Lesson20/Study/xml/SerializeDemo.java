package ru.fedorov.Lesson20.Study.xml;

import javax.xml.bind.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class SerializeDemo {
    public static void main(String[] args) throws JAXBException, FileNotFoundException {
        JAXBContext context = JAXBContext.newInstance(Cat.class, Dog.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

        Cat cat = new Cat("Пушок", 2, new String[]{"мышь", "мячик"});
        cat.setSecret("Ginger");
        marshaller.marshal(cat, System.out);

        Dog dog = new Dog();
        dog.setName("Шарик");
        marshaller.marshal(dog, System.out);

        // чтение XML (десериализация, анмаршаллинг, парсинг)
        Unmarshaller unmarshaller = context.createUnmarshaller();
        InputStream is = new FileInputStream("src/ru/fedorov/lesson20/Study/xml/Cat.xml");
        Cat cat2 = (Cat) unmarshaller.unmarshal(is);
        System.out.println(cat);
        System.out.println(cat2);
    }
}
