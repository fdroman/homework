package ru.fedorov.Lesson20.Study.json;


import com.fasterxml.jackson.annotation.*;

import ru.fedorov.Lesson20.Study.xml.Animal;

import java.util.Arrays;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Cat extends Animal {
    private String secret;
    private int age;
    private String[] games;
    private String color;

    public Cat(String name, int age, String[] games) {
        setName(name);
        this.age = age;
        this.games = games;
    }

    public Cat() {
    }

    @JsonProperty(value = "CatAge")
    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String[] getGames() {
        return games;
    }

    public void setGames(String[] games) {
        this.games = games;
    }

    @JsonIgnore
    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Cat{" +
                "name='" + getName() + '\'' +
                ", age=" + age +
                ", games=" + Arrays.toString(games) +
                ", secret=" + secret +
                ", color=" + color +
                '}';
    }
}
