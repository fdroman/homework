package ru.fedorov.Lesson20.Study.json;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

public class SerializeDemo {
    public static void main(String[] args) throws IOException {
        Cat cat = new Cat("Пушок", 2, new String[]{"мышь", "мячик"});
        cat.setSecret("Ginger");

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writeValue(new File("src/ru/fedorov/lesson20/Study/json/Cat.json"), cat);

        // Получить кота назад
        String s = "{\"name\":\"Пушок\",\"games\":[\"мышь\",\"мячик\"]," +
                "\"CatAge\":9,\"color\":\"рыжий\",\"type\":\"европейская\"}";
        Reader reader = new StringReader(s);
        Cat cat2 = objectMapper.readValue(reader, Cat.class);

        System.out.println(cat);
        System.out.println(cat2);
    }
}
