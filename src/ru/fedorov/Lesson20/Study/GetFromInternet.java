package ru.fedorov.Lesson20.Study;

        import java.io.*;
        import java.net.URL;

public class GetFromInternet {
    public static void main(String[] args) {
        try {
            URL url = new URL("http://api.icndb.com/jokes/random");
            try (InputStream is = url.openStream();
                 Reader reader = new InputStreamReader(is);
                 BufferedReader br = new BufferedReader(reader)
            ){
                System.out.println(br.readLine());

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
