package ru.fedorov.Lesson20.HomeWork;

// Есть набор бесплатных сервисов в интернете, предоставляющих данные в формате JSON:
// https://github.com/toddmotto/public-apis/blob/master/README.md
// Выбрать любой сервис, какой больше нравится, и написать программу, которая с ним взаимодействует.
// Класс сериализуемого объекта может содержать не все поля, а только 2-3 ключевых.
// Например, для погоды достаточно показать диапазон температур.
// Минимальное количество запросов к сервису - 1.
// Не обязательно реализовывать весь функционал, предоставляемый сервисом. Достаточного одного любого запроса

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.net.URL;

public class APP {

    private static  final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private final static String URL_ADDRESS = "http://api.icndb.com/jokes/random";

    public static void main(String[] args) {
        JsonChuckNorris json;
        URL url = null;
        try {
            url = new URL(URL_ADDRESS);
            try (BufferedReader readerURL = new BufferedReader(new InputStreamReader(url.openStream()))) {
                json = OBJECT_MAPPER.readValue(readerURL, JsonChuckNorris.class);
                OBJECT_MAPPER.writeValue(System.out, json);
            }
            System.out.println(json);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
