package ru.fedorov.Lesson20.HomeWork;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JsonChuckNorris {

    private String type;

    @JsonProperty("value")
    private JsonValue jsonValue;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public JsonValue getJsonValue() {
        return jsonValue;
    }

    public void setJsonValue(JsonValue jsonValue) {
        this.jsonValue = jsonValue;
    }
}
// { "type": "success", "value": { "id": 262, "joke":
// "Chuck Norris used to play baseball. When Babe Ruth was hailed as the better player,
// Chuck Norris killed him with a baseball bat to the throat. Lou Gehrig got off easy.",
// "categories": [] } }