package ru.fedorov.Lesson20.HomeWork;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class JsonValue {
    private int id;

    private String joke;

  @JsonIgnore
    private String[] categories;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getJoke() {
        return joke;
    }

    public void setJoke(String joke) {
        this.joke = joke;
    }

    public String[] getCategories() {
        return categories;
    }

    public void setCategories(String[] categories) {
        this.categories = categories;
    }
}
// { "type": "success", "value": { "id": 262, "joke":
// "Chuck Norris used to play baseball. When Babe Ruth was hailed as the better player,
// Chuck Norris killed him with a baseball bat to the throat. Lou Gehrig got off easy.",
// "categories": [] } }