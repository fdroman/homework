package ru.fedorov.Lesson16;

import java.io.*;

public class BigFileDemo {
    private static final int BUFFER_SIZE = 1024;

    public static void main(String[] args) throws IOException {

        InputStream is = new FileInputStream("source.bin");
        OutputStream os = new FileOutputStream("dest");
        try {

            byte[] buf = new byte[BUFFER_SIZE];
            int b;
            while ((b = is.read(buf)) != -1) {
                os.write(buf, 0, b);
            }

        } finally {
            is.close();
            os.close();
        }
    }
}
