package ru.fedorov.Lesson16;

import java.io.*;

public class IODemo {
    public static void main(String[] args) throws IOException {
        OutputStream os = new FileOutputStream("source.bin");
        try {
            os.write(7);
            os.write(2);
            os.write(32);

            byte[] arr = new byte[]{1,0b00000001,6,1,2,3,4,5,77,127, -128};
            os.write(arr);

            byte[] str = "I love Java!".getBytes();
            os.write(str);

        } finally {
            os.close();
        }
    }
}
