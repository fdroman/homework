package ru.fedorov.Lesson16;

import java.io.*;

public class CopyDemo {
    public static void main(String[] args) throws IOException {

        InputStream is = new FileInputStream("source.bin");
        OutputStream os = new FileOutputStream("destination.bin");
        try {

            int b = is.read();
            while (b != -1) {
                os.write(b);
                b = is.read();
            }

        } finally {
            is.close();
            os.close();
        }
    }
}
