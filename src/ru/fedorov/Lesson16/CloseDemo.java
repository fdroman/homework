package ru.fedorov.Lesson16;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

public class CloseDemo {
    private static final Logger LOGGER = LoggerFactory.getLogger(CloseDemo.class);

    public static void main(String[] args) {
        OutputStream os = null;
        try {
            os = new FileOutputStream("closeDemo.bin");
            LOGGER.info("Поток {} успешно открыт", os);
            os.write(7);
            os.write(2);
            os.write(32);
        } catch (IOException e) {
            LOGGER.error("Ошибка при работе с потоком", e);
        } finally {
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                    LOGGER.error("Ошибка при закрытии потока", e);
                }
            }
        }

        // try-with-resources

        try (
                OutputStream os2 = new FileOutputStream("closeDemo.bin")
        ) {
            os2.write(7);
            os2.write(2);
            os2.write(32);
        } catch (IOException e) {
            LOGGER.error("Ошибка при работе с потоком", e);
        }
    }
}
