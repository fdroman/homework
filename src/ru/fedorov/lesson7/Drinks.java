package ru.fedorov.lesson7;

import java.sql.SQLOutput;

public enum Drinks {
//Хотел сделать КОНЕЧНЫЙ объём напитков и зациклить: параметр quantity, но передумал )))))


    WATER(1, 10, "вода  ", 5),
    TEA(2, 10, "чай   ", 10),
    SPRITE(3, 10, "спрайт", 15),
    COLA(4, 10, "кола  ", 20),
    COCOA(5, 10, "какао ", 25),
    COFFEE(6, 10, "кофе  ", 30);


    private int key;
    private int quantity;
    private String drink;
    private int price;

    Drinks(int key, int quantity, String drink, int price) {
        this.key = key;
        this.quantity = quantity;
        this.drink = drink;
        this.price = price;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getKey() {
        return key;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getDrink() {
        return drink;
    }

    public int getPrice() {
        return price;
    }
}


