package ru.fedorov.lesson7;

import java.util.Scanner;

public class WM {
    public int money;
    public int drink;
    public int run;

    public void show() {        //вывод списка на экран
        System.out.println('\n' + "НОМЕР ПУНКТА | НАПИТОК| ЦЕНА");
        for (Drinks s : Drinks.values())
            System.out.println("Напиток № " + s.getKey() + " | " + s.getDrink() + " | " + s.getPrice());
    }

    public int addMoney(int money) {        //приём денег купюроприёмником
        //       System.out.println("пришло на вход: " + money); //для отработки логики
        Scanner scan = new Scanner(System.in);
        String input;
        do {
            System.out.print("Введите купюру в купюроприёмник:");
            int a = scan.nextInt();
            money = money + a;
            System.out.print("Вы ввели " + a + " руб.  ИТОГО= " + money + " руб.    Хотите добавить купюру yes/no ");
            input = scan.next();
        } while (input.equals("yes"));
        this.money = money;
        return money;
    }

    public int addDrink() {     //ввод помера напитка
        for (; ; ) {    //пока не введут число в диапазоне от 1 до 6
            Scanner scan = new Scanner(System.in);
            System.out.print("Введите номер напитка:");
            int drink = scan.nextInt();
            //           System.out.println(drink + " - номер напитка"); //для отработки логики
            if ((drink > 0) && (drink < 7)) {
                this.drink = drink;
                return drink;
            } else {
                System.out.println("такого напитка не существует, корректно вводите данные!");
                WM print = new WM();    //вывод списка напитков на экран
                print.show();
            }
        }
    }

    public void analysis(int money, int drink, int run) {   //анализ всего введённого
        Scanner scan = new Scanner(System.in);
        String input;
        for (Drinks i : Drinks.values())
            if (i.getKey() == drink) {
                if (money >= i.getPrice()) {
                    System.out.println("Угощайтесь напитком: " + i.getDrink() + " Ваша сдача= " + (money - i.getPrice()) + " руб.");
                } else {
                    System.out.println("Для выбранного напитка: " + i.getDrink() + " у Вас недостаточно средств в размере " + (i.getPrice() - money) + " руб.");
                    System.out.print("Хотите пополнить сумму или выбрать другой напиток?  yes/no");
                    input = scan.next();
                    if (input.equals("yes")) {
                        run = 1;
                    } else {
                        run = 0;
                        System.out.println("Возврат введёных денег: " + money + " руб.");
                    }
                    this.run = run;
                }
            }

    }
}
