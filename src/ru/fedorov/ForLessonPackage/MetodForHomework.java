package ru.fedorov.ForLessonPackage;

/*
 * Сюда будут добавлятся методы по мере необходимости
 */

import java.time.LocalDate;
import java.util.Random;
import java.util.Scanner;

public class MetodForHomework {
    private static Scanner scanner = new Scanner(System.in);

    /*
     * метод для ввода int с консоли в диапазоне чисел от A до B включительно
     */

    public static int intFromConsole(String message, int A, int B) {
        int q;
        System.out.print(message);
        while (scanner.hasNext()) {
            if (scanner.hasNextInt()) {
                q = scanner.nextInt();
                if (q >= A & q <= B) { //если число в диапазоне A и B
                    return q;
                } else {
                    System.out.println("Введите число в диапазоне от " + A + " до " + B);
                }
            } else {
                System.out.print("Некорректный ввод (требуется int), повторите ввод:");
                scanner.next();
            }
        }
        return 0;
    }

    /*
     * метод для генерирования N случайных int уникальных чисел от A до B включительно
     */
    public static Object intNumberRandomAB(int arrayNumber, int numberA, int numberB) {
        //System.out.println("Количество:" + arrayNumber + "   от " + numberA + "  до " + numberB);
        int rande = numberB - numberA;
        int[] array = new int[arrayNumber];
        if ((rande + 1) < arrayNumber) {
            System.out.println("заявленное количество целых чисел (" + arrayNumber + ") не помещается в диапазон от " + numberA + " до " + numberB);
            return 0;
        }
        for (int i = 0; i < arrayNumber; i++) {
            array[i] = (numberA + ((int) (Math.random() * (rande + 1))));
            for (int j = 0; j < i; j++) {
                if (array[i] == array[j]) {
                    i = i - 1;
                    break;
                }
            }
            //System.out.println(i + " значение, число= " + array[i]);
        }
        return array;
    }

    /*
     * метод для генерирования даты (год-месяц-число) от года A до года B
     */
    public static Object LocalDateAB(int yearA, int monthA, int dayA, int yearB, int monthB, int dayB) {
        Random random = new Random();
        int minDay = (int) LocalDate.of(yearA, monthA, dayA).toEpochDay();
        int maxDay = (int) LocalDate.of(yearB, monthB, dayB).toEpochDay();
        long randomDay = minDay + random.nextInt(maxDay - minDay);
        return LocalDate.ofEpochDay(randomDay);
    }

    /*
     * Метод для реверса элеметнов в одномерном массиве Lesson21
     */
    public static Object ArrayReverse (int array[]){
            int len = array.length - 1; //количество элементов
            int temp;                   //промежуточная переменная
            int w = (len / 2);          //необходимое количество перемещений в массиве
            for (int i = 0; i <= w; i++) {
                temp = array[i];
                array[i] = array[len - i];
                array[len - i] = temp;
            }
            return array[len-1];
        }
    }


//    private intFromConsole2(String message) {
//        System.out.print(message);
//        while (scanner.hasNext()) {
//            if (scanner.hasNextInt()) {
//                return scanner.nextInt();
//            }
//            System.out.print("Некорректный ввод (требуется int), повторите ввод:");
//            scanner.next();
//        }
//        return 0;
//
//    }


