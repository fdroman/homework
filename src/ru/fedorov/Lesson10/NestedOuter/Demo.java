package ru.fedorov.Lesson10.NestedOuter;

class Demo {
    public static void main(String[] args) {
        NestedOuter.NestedInner nd = new NestedOuter.NestedInner();
        nd.print();
        NestedOuter.NestedInner.staticPrint();
    }
}
