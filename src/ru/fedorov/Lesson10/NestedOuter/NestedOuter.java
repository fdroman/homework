package ru.fedorov.Lesson10.NestedOuter;

public class NestedOuter {
    private static int i = 1;
    private int ii = 11;

    static class NestedInner {
        void print() {
            System.out.println("i'm static nested!!!");
            System.out.println(i);
        }

        static void staticPrint() {
            System.out.println("i'm static nested!!!");
            NestedOuter no = new NestedOuter();
            System.out.println(no.ii);
        }
    }
}
