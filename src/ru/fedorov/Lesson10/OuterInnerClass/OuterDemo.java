package ru.fedorov.Lesson10.OuterInnerClass;

class OuterDemo {
    private int num = 42;
    // Внутренний класс
    public class InnerDemo {
        private int num = 24;
        public void print() {
            System.out.println("Это внутренний класс и ему доступно private поле внешнего класса!");
            System.out.println(num);
            System.out.println(OuterDemo.this.num);
        }
    }
    // Доступ к внутре ннему классу из метода
    void displayInner() {
        InnerDemo inner = new InnerDemo();
        inner.print();
        System.out.println("-=-");
    }
}

