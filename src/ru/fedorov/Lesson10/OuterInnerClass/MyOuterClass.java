package ru.fedorov.Lesson10.OuterInnerClass;

public class MyOuterClass {
    public static void main(String args[]) {
        // Создание внешнего класса
        OuterDemo outer = new OuterDemo();
        System.out.println("11111");
        // Доступ к методу displayInner()
        outer.displayInner();
        OuterDemo.InnerDemo innerDemo = outer.new InnerDemo();
        System.out.println("22222");
        innerDemo.print();
    }
}