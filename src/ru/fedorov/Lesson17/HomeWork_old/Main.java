package ru.fedorov.Lesson17.HomeWork_old;

import ru.fedorov.ForLessonPackage.MetodForHomework;
import ru.fedorov.Lesson17.HomeWork.Book;
import ru.fedorov.Lesson17.HomeWork.Library;

import java.io.*;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Random;
import java.util.stream.Stream;

public class Main {
    private static final String WAY = "src/ru/fedorov/Lesson17/HomeWork_old/Library.txt";

    public static void main(String[] args) throws Exception {

        ru.fedorov.Lesson17.HomeWork.Book book = addBook();
        write(book);
        ru.fedorov.Lesson17.HomeWork.Library library = load();

        System.out.println("Добавление кники в библиотеку: \n" + book + "\n");
        System.out.print("Литература в библиотеке: \n");

//        Stream var10000 = Arrays.stream(library.getBooks()).filter((b) -> {
//            //System.out.println("цикл");
//            return b != null;
//        });
//        PrintStream var10001 = System.out;
//        var10000.forEach(var10001::println);




        Arrays.stream(library.getBooks()).filter(b -> b != null).forEach(System.out::println);
    }

//    private static void forEach(Object o) {
//    }

    private static ru.fedorov.Lesson17.HomeWork.Book addBook() {
        ru.fedorov.Lesson17.HomeWork.Book book = new ru.fedorov.Lesson17.HomeWork.Book();
        book.setBookName("книга" + (int) (Math.random() * 1000));
        book.setAuthor("Автор YYY");
        book.setMadeYear((LocalDate) MetodForHomework.LocalDateAB(1900, 1, 1, 2019, 6, 14));

        return book;
    }

    private static void write(ru.fedorov.Lesson17.HomeWork.Book book) throws IOException {
        File file = new File(WAY);
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            if (file.exists()) {
                //System.out.println("файл есть");
                fos = new FileOutputStream(file, true);
                oos = new AppendingObjectOutputStream(fos);
            } else {
                //System.out.println("файла нет");
                fos = new FileOutputStream(file, true);
                oos = new ObjectOutputStream(fos);
            }
            oos.writeObject(book);

        } finally {
            oos.close();
            fos.close();
        }
    }

    private static ru.fedorov.Lesson17.HomeWork.Library load() throws IOException, ClassNotFoundException {
        //System.out.println("-=-");
        FileInputStream fis = new FileInputStream(WAY);
        ObjectInputStream ois = new ObjectInputStream(fis);
        ru.fedorov.Lesson17.HomeWork.Book[] books = new ru.fedorov.Lesson17.HomeWork.Book[100]; //всего книг в библиотеке
        ru.fedorov.Lesson17.HomeWork.Library library = new Library();
        int i = 0;
        try {
            while (fis.available() != 0) {
                ru.fedorov.Lesson17.HomeWork.Book book = (Book) ois.readObject();
                //System.out.println("book "+ book);
                books[i] = book;
                i++;
            }
            //System.out.println("library.setBooks(books) "+ books[i-1]);
            library.setBooks(books);
        } finally {
            ois.close();
            fis.close();
        }
        return library;
    }

}
