package ru.fedorov.Lesson17.Study.adapter.easy;

public class AnimalAdapter extends Animal {
    private Robot robot;

    public AnimalAdapter(Robot robot) {
        this.robot = robot;
    }

    @Override
    public void walk() {
        robot.drive();
    }
}
