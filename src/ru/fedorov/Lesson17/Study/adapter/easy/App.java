package ru.fedorov.Lesson17.Study.adapter.easy;

public class App {
    public static void main(String[] args) {
        Robot robot = new Robot();
        doWalk(new AnimalAdapter(robot));
    }

    private static void doWalk(Animal animal) {
        animal.walk();
    }

}
