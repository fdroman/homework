package ru.fedorov.Lesson17.Study.stream;

import java.io.*;

public class IODemo {
    public static void main(String[] args) {
        try (OutputStream os = new FileOutputStream("myFile.txt");
             OutputStream bos = new BufferedOutputStream(os);
             PrintStream ps = new PrintStream(bos);
        ) {
            ps.println("Hello, world!!!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
