package ru.fedorov.Lesson17.Study.stream;

import java.io.*;

public class DataDemo {
    public static void main(String[] args) {
        String filename = "Data.bin";

        // Записываем переменные в файл
        try (OutputStream os = new FileOutputStream(filename);
             DataOutputStream dos = new DataOutputStream(os)
        ) {
            dos.writeBoolean(true);
            dos.writeLong(123456789123456789l);
            dos.writeChar('M');
            dos.writeInt(17);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // доставать переменные из файла
        try (InputStream fis = new FileInputStream(filename);
             DataInputStream dis = new DataInputStream(fis)
        ) {

            boolean areYouMerry = dis.readBoolean();
            long callories = dis.readLong();
            String sex = ""+dis.readChar();
            int age = dis.readInt();

            System.out.println(areYouMerry);
            System.out.println(callories);
            System.out.println(sex);
            System.out.println(age);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
