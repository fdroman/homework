package ru.fedorov.Lesson17.Study.serialize;

import java.io.Serializable;

public class Student implements Serializable {
    private static final long serialVersionUID = 6L;

    static String cl = "Студент";

    private String name;
    private int age;
    private int num;
    private transient String secret;

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", num=" + num +
                ", secret=" + secret +
                '}';
    }
}
