package ru.fedorov.Lesson17.Study.serialize;

import java.io.*;

public class SerializeDemo {
    private static final String fileName = "Student.bin";

    public static void main(String[] args) throws Exception {
        Student student = new Student("Иван", 26);
        student.setNum(123);
        student.setSecret("Pin-CODE");
        save(new Student[]{student});
        System.out.println("Был:  " + student);

        Student stu2 = load();

        System.out.println("Стал: " + stu2);
    }

    private static Student load() throws IOException, ClassNotFoundException {
        try (ObjectInputStream ois = new ObjectInputStream(
                new FileInputStream(fileName)
        )) {
            return (Student) ois.readObject();
        }
    }

    private static void save(Student[] students) {
        try (ObjectOutputStream oos = new ObjectOutputStream(
                new FileOutputStream(fileName)
        )) {
            oos.writeObject(students);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
