package ru.fedorov.Lesson17.Study.wrapper.hard;

public class CatWrapper implements Cat {
    Cat cat;

    public CatWrapper(Cat cat) {
        //super(cat.getName());
        this.cat = cat;
    }

    @Override
    public String getName() {
        return cat.getName()+"-кот";
    }
}
