package ru.fedorov.Lesson17.Study.wrapper.hard;

public final class RealCat implements Cat {

    private String name;

    public RealCat(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
