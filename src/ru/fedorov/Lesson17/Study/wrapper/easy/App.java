package ru.fedorov.Lesson17.Study.wrapper.easy;

public class App {
    public static void main(String[] args) {
        Cat cat = new Cat();
        System.out.println(cat.say());

        Cat ncat = new CatWrapper(cat);
        System.out.println(ncat.say());
    }
}
