package ru.fedorov.Lesson17.HomeWork;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import static ru.fedorov.Lesson17.HomeWork.App.WAY;

public class Reading  {
    public Library read() throws IOException, ClassNotFoundException {
        //private static Library load () throws IOException, ClassNotFoundException
        System.out.println("я в class Reading");
                FileInputStream fis = new FileInputStream(WAY);
                ObjectInputStream ois = new ObjectInputStream(fis);
                Book[] books = new Book[100];
                Library library = new Library();
                int i = 0;
                try {
                    while (fis.available() != 0) {
                        Book book = (Book) ois.readObject();
                        books[i] = book;
                        i++;
                    }
                    library.setBooks(books);
                } finally {
                    ois.close();
                    fis.close();
                }
                return library;
            }
        }

