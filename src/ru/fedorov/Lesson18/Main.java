package ru.fedorov.Lesson18;

//Написать программу, которая копирует файл с одной кодировкой в файл с другой..

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class Main {
    public static final String FILE_UTF8 = "src/ru/fedorov/lesson18/UTF-8.txt";
    public static final String FILE_US_ASCII = "src/ru/fedorov/lesson18/US-ASCII.txt";
    public static final String FILE_UTF_16LE = "src/ru/fedorov/lesson18/UTF_16LE.txt";

    public static void main(String[] args) throws IOException {

        try (FileWriter writer = new FileWriter(FILE_UTF8)) {
            String text = "HELLO WORLD!\nМИРУ МИР!";
            writer.write(text);
            writer.flush();

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        File f1 = new File(FILE_UTF8);
        File f2 = new File(FILE_US_ASCII);
        File f3 = new File(FILE_UTF_16LE);

        int a;
        BufferedWriter bw1 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(f2), StandardCharsets.US_ASCII));
        BufferedWriter bw2 = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(f3), StandardCharsets.UTF_16LE));
        FileReader fr1 = new FileReader(f1);
        FileReader fr2 = new FileReader(f1);

//
//        InputStream is = new FileInputStream(FILE_UTF8);
//        byte[] buf = new byte[100];
//        if(is.read(buf) != -1){
//            String s = new String(buf, "ASCII");
//            System.out.print(s);
//        }

        while ((a = fr1.read()) != -1) {
            bw1.write(a);
            System.out.print("-"+a);
        }
        System.out.println();
        while ((a = fr2.read()) != -1) {
            bw2.write(a);
            System.out.print("-"+a);
        }
        fr1.close();
        fr2.close();
        bw1.close();
        bw2.close();
    }
}

