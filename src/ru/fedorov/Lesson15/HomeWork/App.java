package ru.fedorov.Lesson15.HomeWork;

//работа с файлами. Всё сделано в одном классе для наглядности.

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class App {

    public static final String WAY = "src/ru/fedorov/lesson15/HomeWork/dirLesson15";
    public static final String FILE1 = "text1.txt";
    public static final String FILE2 = "src/ru/fedorov/lesson15/HomeWork/dirLesson15/text2.txt";
    public static final String FILE3 = "src/ru/fedorov/lesson15/HomeWork/dirLesson15/text3.txt";
    public static final String FILE4COPY = "src/ru/fedorov/lesson15/HomeWork/dirLesson15/text4Copy.txt";


    public static void main(String[] args) {

        //создали дирректорию src/ru/fedorov/lesson15/HomeWork/dirLesson15
        File dir = new File(WAY);
        if (dir.mkdir()) {
            System.out.println("создали директорию dirLesson15");
        } else {
            System.out.println("директория dirLesson15 уже существует");
        }

        //создали файл первым методом
        File file = new File(dir, FILE1);
        try {
            if (file.createNewFile()) {
                System.out.println("создан файл text1.txt");
            } else {
                System.out.println("файл text1.txt уже сужествует");
            }
        } catch (IOException e) {
            System.out.println("Ошибка в создании файла text.txt по причине: " + e.getMessage());
        }

        //создали файл вторым методом
        Path path = Paths.get(FILE2);
        try {
            Files.createFile(path);
            System.out.println("создан файл text2.txt");
        } catch (IOException e) {
            System.out.println("файл tex2.txt уже сужествует");
        }

        //переименовываем файл
        File file3 = new File(FILE3);
        if (new File(FILE2).renameTo(new File(FILE3))) {
            System.out.println("Файл text2.txt переименован в text3.txt");
        } else {
            file3.delete();
            new File(FILE2).renameTo(new File(FILE3));
            System.out.println("Файл text2.txt переименован в txt3.txt после удаления старого text3.txt");
        }

        //копируем файл
        try {
            Files.copy(Paths.get(FILE3), Paths.get(FILE4COPY));
            System.out.println("text3.txt скопировали в text4Copy.txt");
        } catch (Exception e) {
            System.out.println("файл text3.txt не скопирован в text4Copy.txt (text4Copy.txt - уже существует)");
        }

        //рекурсия
        File dir2 = new File(WAY + "/Папка2");  //создали папку с файлом для отладки глубины рекурсии
        dir2.mkdir();
        String way2 = dir2 + "/text.txt";
        try {
            new File(way2).createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        int q = 1;  //переменная глубины рекурсии
        recursiveMetod((new File(WAY)), q);
        System.out.println("Файлы переименованы с параметром глубины рекурсии");
    }

    private static void recursiveMetod(File dir, int q) {
        File[] files = dir.listFiles();
        for (File file : files) {
            if (file.isFile()) {
                //переименование файла с присвоением цифры перед именем файла о глубине рекурсии
                String name = file.getParent() + File.separator + q + file.getName();
                file.renameTo(new File(name));
            } else {
                q = q + 1; // увеличение глубины рекурсии
                recursiveMetod(file, q);
            }
        }
    }
}


