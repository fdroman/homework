package ru.fedorov.Lesson15.Study;

import java.io.File;
import java.io.IOException;

public class RecurseDemo {
    public static void main(String[] args) throws IOException {
        System.out.println("Создаю иерархию каталогов и файлов");
        new File("dir/a/b").mkdirs();
        new File("dir/d.txt").createNewFile();
        new File("dir/e.txt").createNewFile();
        new File("dir/a/b/2.txt").createNewFile();
        new File("dir/a/b/3.txt").createNewFile();
        new File("dir/a/b/4.txt").createNewFile();

        File dir = new File("dir");
        recursiveDelete(dir);
        System.out.println("Все каталоги удалены");
    }

    private static void recursiveDelete(File dir) {
        File[] files = dir.listFiles();
        for (File file : files) {
            if (file.isFile()) {
                file.delete();
                System.out.println("файл <" + file + "> удален");
            } else {
                // каталог
                recursiveDelete(file);
            }
        }
        dir.delete();
        System.out.println("каталог <" + dir + "> удален");
    }
}
