package ru.fedorov.Lesson15.Study;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class FileDemo {
    public static void main(String[] args) {
        System.out.println(File.separator);
        File file = new File("test.txt");
        try {
            System.out.println(file.createNewFile());
        } catch (IOException e) {
            System.out.println("Не удалось создать файл: " + e.getMessage());
        }

        System.out.println("=== создание каталога ===");
        File dir = new File("../src/dir/a/b");
        System.out.println("Каталог? " + dir.isDirectory());
        System.out.println("Файл?    " + dir.isFile());
        System.out.println("Создан каталог: " + dir.mkdir());
        System.out.println("Каталог? " + dir.isDirectory());
        System.out.println("Файл?    " + dir.isFile());
        if( !dir.isDirectory() ){
            System.out.println("Вложенный каталог создан: " + dir.mkdirs());
        }

        System.out.println("=== сверка путей ===");

        File file1 = new File("../fs/test.txt");
        System.out.println("относительный путь: " + file1.getPath());
        System.out.println("абсолютный путь:    " + file1.getAbsolutePath());
        try {
            System.out.println("канонический путь:  " + file1.getCanonicalPath());
        } catch (IOException e) {
            System.out.println("Невозможно получить канонический путь");
        }

        System.out.println(file.equals(file1)); // false
        System.out.println(file.getAbsolutePath().equals(file1.getAbsolutePath())); // false
        try {
            System.out.println(file.getCanonicalPath().equals(file1.getCanonicalPath())); // true
        } catch (IOException e) {
            System.out.println("Невозможно получить канонический путь");
        }

        System.out.println("=== список файлов в каталоге ===");
        File cur = new File(".");
        System.out.println(Arrays.toString(cur.list()));
        System.out.println(cur.getTotalSpace());
        System.out.println(cur.getFreeSpace());

        System.out.println("=== переименовать файл ===");
        File newFile = new File("test2.txt");
        System.out.println(file.renameTo(newFile));

        System.out.println("=== удаление файлов и каталогов ===");
        System.out.println("удаление " + file.getName() + ": " + file.delete());
        System.out.println("удаление " + newFile.getName() + ": " + newFile.delete());
        System.out.println("удаление " + dir.getName() + ": "
                + new File("dir").delete());
    }
}
