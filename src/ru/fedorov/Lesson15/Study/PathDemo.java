package ru.fedorov.Lesson15.Study;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class PathDemo {
    public static void main(String[] args) throws IOException {
        // первый способ создания Path
        //File file = new File("MyFile.txt");
        //Path path = file.toPath();

        Path path = Paths.get("C:/Users/Роман/IdeaProjects/work/src/ru/fedorov/12345.txt");
        System.out.println("absolute: " + path.isAbsolute());
        System.out.println("root: " + path.getRoot());
        System.out.println("parent: " + path.getParent());
        System.out.println("uri: " + path.toUri());

        //Files
        //Files.createFile(path);
        System.out.println(Files.readAllLines(path));

    }
}
