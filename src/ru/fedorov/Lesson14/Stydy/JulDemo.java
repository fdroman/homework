package ru.fedorov.Lesson14.Stydy;

import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JulDemo {
    public static final Logger logger = Logger.getLogger (JulDemo.class.getName());
    public static void main(String[] args) throws FileNotFoundException {
        //System.err.println("Начало работы программы");
        logger.log(Level.FINE,"Начало работы программы");
        try {
            doSome("значение");
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.getMessage(), e);
            throw new RuntimeException(e);
//        } finally {
//            System.err.println("сейчас будет исключение");
//            throw new RuntimeException("давим все исключения");
        }

       System.out.println("Пока");
        logger.fine("Программа завершена");
    }

    private static void doSome(String value) {
        logger.fine("вызван метод doSome c параметром <" + value + ">");

        throw new RuntimeException("Какая-то ошибка");
    }
}