package ru.fedorov.Lesson14.Stydy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.io.FileNotFoundException;
import java.util.Scanner;

public class Slf4jDemo {
    private static final Logger logger = LoggerFactory.getLogger(Slf4jDemo.class);

    public static void main(String[] args) throws FileNotFoundException {
        logger.info("Начало работы программы");
        String user = authorize();
        MDC.put("user", user);
        try {
            doSome("значение");
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        System.out.println("Пока!");
        logger.info("Программа завершена");
    }
    /**
     * Некоторая логика по авторизации пользователя
     * @return имя пользователя
     */
    private static String authorize() {
        Scanner scanner = new Scanner(System.in);
        String user = scanner.nextLine();
        return user;
    }
    private static void doSome(String value) {
        logger.info("Вызван метод doSome с параметром <{}> от имени пользователя <{}>",
                value, MDC.get("user"));
        logger.warn("сейчас будет ошибка");
        throw new RuntimeException("Какая-то ошибка");
    }
}

