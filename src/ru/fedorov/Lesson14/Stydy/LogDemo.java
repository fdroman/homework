package ru.fedorov.Lesson14.Stydy;

import java.io.FileNotFoundException;
import java.io.PrintStream;

public class LogDemo {
    public static void main(String[] args) throws FileNotFoundException {
    System.setErr(new PrintStream("log.txt"));

    System.err.println("Начало работы программы");
    try {
        doSome("значение");
    } catch (Exception e) {
        e.printStackTrace();
        throw new RuntimeException(e);
    } finally {
        System.err.println("сейчас будет исключение");
        throw new RuntimeException("давим все исключения");
    }

//        System.out.println("Пока");
//        System.err.println("Программа завершена");
}

    private static void doSome(String value) {
        System.err.println("вызван метод doSome c параметром <" + value + ">");

        throw new RuntimeException("Какая-то ошибка");
    }
}
