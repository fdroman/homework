package ru.fedorov.Lesson14.HomeWork;
//

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class App {
    private static final Logger logger = LoggerFactory.getLogger(ru.fedorov.Lesson14.HomeWork.App.class);

    public static void main(String[] args) {
        logger.info("Начало работы программы ВЕНДИНГОВЫЙ АВТОМАТ");

        int money = 0;
        int drink = 1;
        int run = 0;

        System.out.println("    ВЕНДИНГОВЫЙ АВТОМАТ");
        WM print = new WM();    //вывод списка на экран
        print.show();
        do {
            //           System.out.println("деньги: "+ money);  //что на входе по деньгам
            //           System.out.println("напиток:"+drink);   //что на входе по напиткам
            //           System.out.println("RUN:"+run);         //что на входе по циклам

            WM mon = new WM();      //запрос ввода купюр
            mon.addMoney(money);
            money = mon.money;
            //logger.info("пользователь ввёл купюру: " + String.valueOf(money));

            WM dr = new WM();       //запрос номера напитка
            dr.addDrink();
            drink = dr.drink;
            //logger.info("пользователь ввел номера напитка: " + String.valueOf(drink));

            WM an = new WM();       //анализ
            an.analysis(money, drink, run);
            money = mon.money;
            run = an.run;

        } while (run == 1); //денег не хватило на напиток и решил продолжить ввод купюр
        logger.info("завершение работы программы");
    }
}
