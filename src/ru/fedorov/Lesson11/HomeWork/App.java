package ru.fedorov.Lesson11.HomeWork;

public class App {
    public static void main(String[] args) {
        String s = "Напишите программу которая получает на вход некую строку, \n" +
                "после она вызывает метод, заменяющий в строке все вхождения \n" +
                "слова «бяка» на «вырезано цензурой» и выводит результат в консоль!\n";
        System.out.println("БЫЛО:\n" + s);
        String s2 = s.replaceAll("бяка", "вырезано цензурой");
        System.out.println("СТАЛО:\n" + s2);
    }
}
