package ru.fedorov.Lesson9.paragraph1;
//
public abstract class Animal {
    public abstract void getName();

    public static void main(String[] args) {
        Animal fish = new Fish();
        Animal bird = new Bird();
        Animal mammals = new Mammals();

        fish.getName();
        bird.getName();
        mammals.getName();
    }
}
