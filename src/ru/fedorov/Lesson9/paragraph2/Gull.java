package ru.fedorov.Lesson9.paragraph2;

public class Gull implements Flyeble, Runeble, Swimeble{
    @Override
    public void fly() {
        System.out.println("ЛЕТАЮ");
    }

    @Override
    public void run() {
        System.out.println("БЕГАЮ");
    }

    @Override
    public void swim() {
        System.out.println("ПЛАВАЮ");
    }
}
