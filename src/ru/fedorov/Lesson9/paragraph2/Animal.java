package ru.fedorov.Lesson9.paragraph2;
//
public abstract class Animal {
    private static Object Hippopotamus = "Hippopotamus";
    private static Object Goose = "Goose";
    private static Object Gull = "Gull";

    public abstract void getName();

    public static void main(String[] args) {
        Animal fish = new Fish();
        Animal bird = new Bird();
        Animal mammals = new Mammals();

        fish.getName();
        bird.getName();
        mammals.getName();

        Goose goose = new Goose();
        System.out.println('\n' + "-" + Goose);
        goose.fly();
        goose.swim();

        Hippopotamus hippopotamus = new Hippopotamus();
        System.out.println('\n' + "-" + Hippopotamus);
        hippopotamus.run();
        hippopotamus.swim();

        Gull gull = new Gull();
        System.out.println('\n' + "-" + Gull);
        gull.fly();
        gull.run();
        gull.fly();
    }
}
