package ru.fedorov.Lesson9.paragraph3;

public abstract class Human {
    public static void main(String[] args) {
        Runner runner = new Runner();
        System.out.println("Стиль RUN: "+ runner.styleRun()+ ", скорость: "+ runner.speedRun()+ " км/ч");


        Swimmer swimmer = new Swimmer();
        System.out.println("Стиль SWIM: "+ swimmer.styleSwim()+ ", скорость: "+ swimmer.SpeedSwim()+ " км/ч");
    }
}
