package ru.fedorov.Lesson9.paragraph3;

public interface Run {
    int speedRun();
    String styleRun();
}
