package ru.fedorov.Lesson9.paragraph3;

public class Runner extends Human implements Run {
    @Override
    public int speedRun() {
        return 7;
    }

    @Override
    public String styleRun() {
        return "спортивная ходьба";
    }
}
