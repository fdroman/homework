package ru.fedorov.Lesson12.Car;

public class EngineException extends Exception {
    public EngineException(String message) {
        super(message);
    }
}
