package ru.fedorov.Lesson12.Car;

public class EngineNotStartedException extends EngineException {

    public EngineNotStartedException(String message) {
        super(message);
    }
}
