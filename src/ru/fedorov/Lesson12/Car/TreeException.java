package ru.fedorov.Lesson12.Car;

public class TreeException extends RuntimeException {
    public TreeException(String message) {
        super(message);
    }
}
