package ru.fedorov.Lesson12.Car;

public class Car {

    private Engine engine = new Engine();

    public void startCar() {
        engine.start();
    }

    public void drive(int distance) throws EngineException {
        engine.forsage();
        double situation = Math.random();
        if (situation < 0.5) {
            throw new TreeException("вижу дерево!");
        }
        System.out.println("Проехали " + distance + " км");
    }

    public void stopCar() {
        engine.stop();
    }

    private class Engine {
        private boolean isStarted = false;

        public void start() {
            isStarted = true;
            System.out.println("двигатель запущен");
        }
        public void stop(){
            isStarted = false;
            System.out.println("двигатель остановлен");
        }

        public void forsage() throws EngineException {
            if (!isStarted) {
                // TODO: 2019-05-08 нельзя выполнить форсаж
                throw new EngineNotStartedException("Невозможно выполнить форсаж. Двигатель не заведен. Включите двигатель");
            }
            double situation = Math.random();
            if (situation < 0.2) {
                isStarted = false;
                // TODO: 2019-05-08 кончился бензин
                throw new NoFuelException("Кончился бензин. Двигатель выключен. Заправьте");
            }
            System.out.println("форсаж!");
        }
    }
}