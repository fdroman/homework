package ru.fedorov.Lesson12.Car;

import java.util.Scanner;

public class TrackApp {
    public static void main(String[] args) {

        Car car = new Car();

        Scanner scanner = new Scanner(System.in);
        System.out.println("Выберите действие:");
        System.out.println("start");
        System.out.println("stop");
        System.out.println("drive how_much");

        while (scanner.hasNext()) {
            String action = scanner.next();
            switch (action) {
                case "start": {
                    car.startCar();
                    break;
                }
                case "stop": {
                    car.stopCar();
                    break;
                }
                case "drive": {
                    int i = scanner.nextInt();
                    try {
                        car.drive(i);
                    } catch (EngineException e) {
                        System.out.println(e.getMessage());
                    } catch (TreeException e) {
                        System.out.println(e.getMessage());
                        System.out.println("Объехал");
                    }
                    break;
                }
            }
            scanner.nextLine();
            System.out.println("что дальше?");
        }

    }
}
