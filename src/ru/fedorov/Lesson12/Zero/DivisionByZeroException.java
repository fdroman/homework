package ru.fedorov.Lesson12.Zero;

public class DivisionByZeroException extends Throwable {

    @Override
    public String getMessage() {
        return "Произошло деление на 0";
    }
}
