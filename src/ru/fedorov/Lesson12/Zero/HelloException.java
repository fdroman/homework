package ru.fedorov.Lesson12.Zero;

public class HelloException {

    public static void main(String[] args) {

        try {
            System.out.println(divide( 2, 2));
            System.out.println(divide( 2, -1));
            System.out.println(divide( 2, 0));


        } catch (DivisionByZeroException error) {
            System.out.println(error.getMessage());
        } catch (ArithmeticException error) {
            System.out.println("опаньки!");
            throw new RuntimeException(error);
        }
    }

    /**
     * большой сложный расчет
     */
    private static int divide(int c, int d) throws DivisionByZeroException {
        if (d == 1) {
            DivisionByZeroException exception = new DivisionByZeroException();
            throw exception;
        }
        return c / d;
    }

}
